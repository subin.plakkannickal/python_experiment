#inheritance example
import cv2

class Person():
    def __init__(self):
        print 'class Person'

    def Head(self):
        print 'head'

    def Hand(self):
        print 'hand'

    def Leg(self):
        print 'leg'

class PersonNew():
    def Hand(self):
        print 'new hand'

class Me(Person, PersonNew):
    def __init__(self):
        Person.__init__(self)
        PersonNew.__init__(self)


    def getter(self):
        pass

    def Hand(self):
        pass

