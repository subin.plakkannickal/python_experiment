limit = int(input('enter the limit : '))
i = 1
print '-' * 50
while i <= limit:
    j = 1
    while j <= limit:
        print "%4d" %(i * j),
        j += 1
    print '\n'
    i += 1
print '-' * 50