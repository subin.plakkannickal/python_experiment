#!/usr/bin/env python
while True:
    orderOfMatrixA = input('Enter the order of matrix A\n m and n: ')
    orderOfMatrixB = input('Enter the order of matrix B\n m and n: ')
    if orderOfMatrixA[1] == orderOfMatrixB[0]:
        break
    else:
        continue
print"Enter matrixA:"
matrixA = [[float(j) for i, j in enumerate(input()) if i < orderOfMatrixA[1]] for k in range(orderOfMatrixA[0])]
print"Enter matrixB:"
matrixB = [[float(j) for i, j in enumerate(input()) if i < orderOfMatrixB[1]] for k in range(orderOfMatrixB[0])]

productMatrix = [[0 for i in range(orderOfMatrixB[1])] for k in range(orderOfMatrixA[0])]

for i in range(orderOfMatrixB[1]):
    for j in range(orderOfMatrixA[0]):
        sum = 0
        for k in range(orderOfMatrixB[0]):
            sum += matrixA[i][k] * matrixB[k][j]
        productMatrix[i][j] = sum
print '-' * 50
for i in productMatrix:
    print '+---' * 2 + '+'
    for j in i:
        print "%3d" % j,
    print '\n'
print '-' * 50

def ValueError():
    pass
def cv2():
    pass
cv2.__call__()
