def decorate(func):
    def newfunc():
        print("decorater")
        func()
    return newfunc

def decorate2(func):
    print '2nd decorater'
    return func

@decorate   # second execution
@decorate2  # first execution
def func():
    print 'base function'

# func = decorate(decorate2(func))
func()

