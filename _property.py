class Celsius:
    def __init__(self):
        self._temp = 0

    def ToFahrenheit(self):
        return (self._temp * 1.8) + 32

    def GetTemp(self):
        return self._temp

    def SetTemp(self, value):
        if value < -273:
            raise ValueError("Temperature below -273 is not possible")
        print 'set temperature'
        self._temp = value

    temp = property(GetTemp, SetTemp)
