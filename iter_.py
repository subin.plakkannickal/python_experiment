import time

iterater = range(1000000)
t = time.time()
for i in iterater:
    k = i

forTime =  time.time() - t

t1 = time.time()
_iterater = iter(iterater)
while True:
    try:
        k =  next(_iterater)
    except StopIteration:
        break
whileTime =  time.time() - t1


print 'time for FOR loop: %0.6f \ntime for While loop: %0.6f'%(forTime,whileTime)