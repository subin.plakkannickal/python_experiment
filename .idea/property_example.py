class Account(object):
    """The Account class,
    The amount is in dollars.
    """
    def __init__(self, rate):
        self.__amt = 0
        self.rate = rate

    @property
    def amount1(self):
        "The amount of money in the account"
        print 'amount_1'
        return self.__amt

    @property
    def inr(self):
        "Gives the money in INR value."
        return self.__amt * self.rate

    @property.setter
    def amount(self, value):
        print 'amount_2'
        # if value < 0:
        #     print "Sorry, no negative amount in the account."
        #     return
        # self.__amt = value

if __name__ == '__main__':
    acc = Account(rate=61) # Based on today's value of INR :(
    # acc.amount = 20
    # print("Dollar amount:", acc.amount)
    # print("In INR:", acc.inr)
    acc.amount = -100
    print("Dollar amount:", acc.amount)